module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    tsconfigRootDir: __dirname,
    sourceType: 'module',
  },
  env: {
    node: true,
    jest: true,
  },
  ignorePatterns: ['.eslintrc.js'],
  extends: ['eslint:recommended', 'airbnb-base', 'plugin:@typescript-eslint/recommended', 'prettier'],
  plugins: ['@typescript-eslint', 'prettier', 'import'],
  rules: {
    'prettier/prettier': 'error',
    'import/no-unresolved': 'off', // checks typescript
    'import/named': 'off', // checks typescript
    'no-undef': 'off', // @see https://github.com/eslint/typescript-eslint-parser/issues/437
    '@typescript-eslint/no-empty-interface': 'off',
    '@typescript-eslint/prefer-optional-chain': 'error',
    '@typescript-eslint/no-unused-vars': 'error',
    '@typescript-eslint/ban-ts-comment': 'warn',
    '@typescript-eslint/no-explicit-any': 'warn',
    '@typescript-eslint/interface-name-prefix': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-floating-promises': 'warn',
    '@typescript-eslint/no-misused-promises': 'warn',
    'no-void': [
      'error',
      {
        allowAsStatement: true,
      },
    ],
    'import/extensions': 'off', // unwanted
    'no-useless-constructor': 'off', // unwanted
    'prefer-promise-reject-errors': 'off', // unwanted
    'class-methods-use-this': 'off', // unwanted
    'max-classes-per-file': 'off', // unwanted
    'no-empty-function': ['error', { allow: ['constructors'] }],
    'no-useless-catch': 'off', // unwanted
    'no-restricted-syntax': ['error', 'ForInStatement', 'LabeledStatement', 'WithStatement'],
    'no-param-reassign': 'off', // unwanted
    'no-shadow': 'off', // breaks TS enums, god will ever know why
    'import/order': [
      'error',
      {
        'newlines-between': 'never',
        pathGroupsExcludedImportTypes: ['internal'],
        alphabetize: {
          order: 'asc',
          caseInsensitive: true,
        },
        groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index', 'object'],
        pathGroups: [
          {
            pattern: 'src/**',
            group: 'internal',
            position: 'before',
          },
          {
            pattern: '@{App,Common,prisma}/**',
            group: 'internal',
          },
          {
            pattern: '@{App,Common,prisma}',
            group: 'internal',
            position: 'before',
          },
          {
            pattern: '@/**',
            group: 'internal',
            position: 'after',
          },
          {
            pattern: '@*/*', // scoped packages
            group: 'external',
            position: 'after',
          },
        ],
      },
    ],
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    // Limit Cyclomatic Complexity
    complexity: ['error', { max: 19 }],
    // Naming things
    '@typescript-eslint/naming-convention': [
      'error',
      // all variables, functions and properties follow are camelCase
      { selector: 'variableLike', format: ['camelCase'] },
      // Class members are camelCase
      {
        selector: ['memberLike'],
        format: ['camelCase'],
      },
      // Class names are PascalCase
      { selector: 'class', format: ['PascalCase'] },
      // Enums
      { selector: 'enum', format: ['PascalCase'] },
      { selector: 'enumMember', format: ['PascalCase'] },
      // boolean variables are prefixed with an allowed verb
      {
        selector: 'variable',
        types: ['boolean'],
        format: ['PascalCase'],
        prefix: ['is', 'should', 'has', 'can', 'did', 'will'],
      },
      {
        selector: 'variable',
        modifiers: ['const'],
        format: ['camelCase', 'UPPER_CASE'],
      },
    ],
  },
}
