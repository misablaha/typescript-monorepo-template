export class IPLookup {
  private url = 'https://api.ipify.org'

  private url64 = 'https://api64.ipify.org'

  public async getIP(): Promise<string> {
    // eslint-disable-next-line no-console
    console.log('IPLookup.getIP')
    const response = await fetch(this.url)
    return response.text()
  }

  public async getIP64(): Promise<string> {
    // eslint-disable-next-line no-console
    console.log('IPLookup.getIP64')
    const response = await fetch(this.url64)
    return response.text()
  }
}
