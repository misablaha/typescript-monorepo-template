# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.3](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-lookup@1.2.2...@monorepo/ip-lookup@1.2.3) (2023-07-27)

**Note:** Version bump only for package @monorepo/ip-lookup





## [1.2.2](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-lookup@1.2.1...@monorepo/ip-lookup@1.2.2) (2023-07-27)

**Note:** Version bump only for package @monorepo/ip-lookup





## [1.2.1](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-lookup@1.2.0...@monorepo/ip-lookup@1.2.1) (2023-07-27)


### Bug Fixes

* description ([8132f84](https://gitlab.com/misablaha/typescript-monorepo-template/commit/8132f8494c74471dd769903ef33dfe9a8fea3c03))





# [1.2.0](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-lookup@1.1.0...@monorepo/ip-lookup@1.2.0) (2023-07-16)

### Features

- add forgotten index.js and update README.md ([784794d](https://gitlab.com/misablaha/typescript-monorepo-template/commit/784794d23435fa2d65672f620d97ba3ea105ede8))

# [1.1.0](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-lookup@1.0.0...@monorepo/ip-lookup@1.1.0) (2023-07-16)

### Features

- add ip64 support ([a49dedb](https://gitlab.com/misablaha/typescript-monorepo-template/commit/a49dedb4710193cab756f393ad16ecfa63bec2e7))

# 1.0.0 (2023-07-16)

**Note:** Version bump only for package @monorepo/ip-lookup
