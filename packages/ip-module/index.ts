// index.js (index.ts for TypeScript) is the default entry point for a package.
// This is quite a hack ...
// - we want to use this package in other parts of the monorepo
// - standard way to do this is using of fields "main" and "types" in package.json
// - unfortunately, in this case IDE uses *.d.ts files instead of *.ts files
// - so, we must build the package before using it in other parts of the monorepo
// using of /index.ts for TypeScript and /index.js for JavaScript is a workaround
// ... not good, not clean, but it works
export * from './src'
