# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.1.0](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-module@1.0.6...@monorepo/ip-module@1.1.0) (2023-07-28)


### Features

* test version ([a25a3ae](https://gitlab.com/misablaha/typescript-monorepo-template/commit/a25a3ae570a7302ff2ef271acbf8b1a296f85465))





## [1.0.6](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-module@1.0.5...@monorepo/ip-module@1.0.6) (2023-07-27)

**Note:** Version bump only for package @monorepo/ip-module





## [1.0.5](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-module@1.0.4...@monorepo/ip-module@1.0.5) (2023-07-27)

**Note:** Version bump only for package @monorepo/ip-module





## [1.0.4](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-module@1.0.3...@monorepo/ip-module@1.0.4) (2023-07-27)

**Note:** Version bump only for package @monorepo/ip-module





## [1.0.3](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-module@1.0.2...@monorepo/ip-module@1.0.3) (2023-07-16)

**Note:** Version bump only for package @monorepo/ip-module

## [1.0.2](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-module@1.0.1...@monorepo/ip-module@1.0.2) (2023-07-16)

**Note:** Version bump only for package @monorepo/ip-module

## [1.0.1](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@monorepo/ip-module@1.0.0...@monorepo/ip-module@1.0.1) (2023-07-16)

### Bug Fixes

- typo ([fb66bbb](https://gitlab.com/misablaha/typescript-monorepo-template/commit/fb66bbbfaca4cf294282435502d26dcf3bf58321))

# 1.0.0 (2023-07-16)

**Note:** Version bump only for package @monorepo/ip-module
