// Entry point to the utils package for JavaScript
// why "./dist/src" instead of "./dist"?
// - because we have an "index.js" file in the root folder
// - and because of it is not possible to have any TS file out of the TypeScript root folder
// - we needed to set "rootDir": "." in the tsconfig.json
// - related to this - content of the root folder is copied to the "dist" folder
// - it means "src" folder is copied to the "dist" folder
module.exports = require('./dist/src')
