import { IPLookup } from '@monorepo/ip-lookup'
import { DynamicModule, Global, Module, Type } from '@nestjs/common'
import { UtilsBaseService } from './utils-base.service'
import { UTILS_SERVICE_TOKEN } from './utils.constants'
import { IUtilsService } from './utils.interface'

// UtilsCoreModule is a private module that is not exported from package
// UTILS_SERVICE_TOKEN is the only one exported to the global scope
// It carries the service class that can be reexported by UtilsModule
@Global()
@Module({})
export class UtilsCoreModule {
  static forRoot = <T extends IUtilsService>(serviceClass: Type<T>): DynamicModule => ({
    module: UtilsCoreModule,
    providers: [
      {
        provide: UTILS_SERVICE_TOKEN,
        useClass: serviceClass,
      },
      // Everything that is used as dependency must be provided
      // IPLookup is used by UtilsBaseService
      IPLookup,
      // UtilsBaseService is used by UtilsMemoizedService
      UtilsBaseService,
    ],
    exports: [UTILS_SERVICE_TOKEN],
  })
}
