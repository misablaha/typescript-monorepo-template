export abstract class IUtilsService {
  abstract sayHello(): string

  abstract getIP(): Promise<string>
}
