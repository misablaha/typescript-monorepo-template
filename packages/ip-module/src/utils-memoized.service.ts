import memoize from 'memoizee'
import { Injectable } from '@nestjs/common'
import { UtilsBaseService } from './utils-base.service'
import { IUtilsService } from './utils.interface'

@Injectable()
export class UtilsMemoizedService implements IUtilsService {
  getIP: () => Promise<string>

  constructor(private base: UtilsBaseService) {
    this.getIP = memoize(this.base.getIP.bind(this.base), { promise: true, maxAge: 10000 }) // 10 seconds
  }

  sayHello(): string {
    return 'Hello from UtilsMemoizedService'
  }
}
