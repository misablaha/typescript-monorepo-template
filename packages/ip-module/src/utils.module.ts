import { DynamicModule, Module, Type } from '@nestjs/common'
import { UtilsCoreModule } from './utils-core.module'
import { UTILS_SERVICE_TOKEN } from './utils.constants'
import { IUtilsService } from './utils.interface'

@Module({
  providers: [
    {
      provide: IUtilsService,
      // UTILS_SERVICE_TOKEN is a private symbol provided by UtilsCoreModule
      // It is not available unless UtilsCoreModule is imported wia UtilsModule.forRoot()
      useExisting: UTILS_SERVICE_TOKEN,
    },
  ],
  exports: [IUtilsService],
})
export class UtilsModule {
  // Initialize UtilsModule with the required service class
  // This must be called in base AppModule before using UtilsModule in other modules
  static forRoot = <T extends IUtilsService>(serviceClass: Type<T>): DynamicModule => ({
    module: UtilsModule,
    // Initialize UtilsCoreModule with the required service class
    // This will make UTILS_SERVICE_TOKEN available in the global scope
    // It does not look well, but this is the only way how to make
    // the UTILS_SERVICE_TOKEN visible in the decorator of this module
    imports: [UtilsCoreModule.forRoot(serviceClass)],
  })
}
