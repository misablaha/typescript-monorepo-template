import { IPLookup } from '@monorepo/ip-lookup'
import { Injectable } from '@nestjs/common'
import { IUtilsService } from './utils.interface'

@Injectable()
export class UtilsBaseService implements IUtilsService {
  constructor(private ipLookup: IPLookup) {}

  sayHello(): string {
    return 'Hello from UtilsBaseService'
  }

  getIP(): Promise<string> {
    return this.ipLookup.getIP()
  }
}
