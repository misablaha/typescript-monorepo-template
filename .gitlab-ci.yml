stages:
  - test
  - build
  - deploy

variables:
  NODE_BARE_IMAGE: node:18.12.1-alpine
  IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA

install:
  image: $NODE_BARE_IMAGE
  stage: .pre
  cache:
    policy: pull-push
    key:
      files: [yarn.lock]
    paths: ['**/node_modules']
  script:
    - yarn install --frozen-lockfile --no-progress
  rules:
    # if yarn.lock changed
    - changes:
        paths: [yarn.lock]
        compare_to: 'refs/heads/$CI_DEFAULT_BRANCH'

lint:
  image: $NODE_BARE_IMAGE
  stage: test
  cache:
    policy: pull
    key:
      files: [yarn.lock]
    paths: ['**/node_modules']
  script:
    - yarn lint
  rules:
    # commit to master or merge request to master
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH

# On every commit to master
# - bump version using lerna
# - create merge request to master with release commit message
release_setup:
  stage: deploy
  image: node:18.12.1
  script:
    - bash scripts/ci_release_setup.sh
  rules:
    # commit to master with release commit message
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE !~ /release/

# On merge request to master with release commit message (from release_setup)
# - build and publish base docker image
release_build:
  stage: deploy
  image: docker:20.10
  services:
    - docker:20.10-dind
  script:
    - echo "Prepare release at merge request"
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - find . -name "package.json" -o -name "yarn.lock" | tar -cf package.json.tar -T -
    - docker build --pull -t $IMAGE .
    - docker push $IMAGE
  rules:
    # merge request to master with release commit message
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /release/

# On accept merge request to master (created by release_setup, prebuild by release_build)
# - tag commit with version tags
release_tag:
  stage: deploy
  image: node:18.12.1
  script:
    - bash scripts/ci_release_tag.sh
  rules:
    # commit to master with release commit message
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /release/

# On tag commit with version tags
# - finish docker image prepared by release_build and publish
release_publish:
  stage: deploy
  image: docker:20.10
  services:
    - docker:20.10-dind
  script:
    - echo "Finish docker image and publish"
  rules:
    # there is a version tag
    - if: $CI_COMMIT_TAG =~ /[0-9]+\.[0-9]+\.[0-9]+$/
