#!/bin/bash -xe

git config --global user.email "gitlab-ci@localhost"
git config --global user.name "GitLab CI"
CI_REPOSITORY_URL="https://gitlab-ci:${CI_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"

# get all changed package.json files
PACKAGES=$(git diff --name-only HEAD~1 | grep package.json)

# iterate over all changed package.json files
for PACKAGE in $PACKAGES; do
  TAG=$(node -p "p=require('./$PACKAGE'); p.name + '@' + p.version")
  # check if tag already exists
  if git rev-parse "$TAG" >/dev/null 2>&1; then
    echo "Version $TAG already exists"
    continue
  fi
  echo "Publishing $TAG"
  git tag -a "$TAG" -m "$TAG"
done

# push all tags
git push "${CI_REPOSITORY_URL}" --tags
