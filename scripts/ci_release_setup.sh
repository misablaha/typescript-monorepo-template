#!/bin/bash -xe

git config --global user.email "gitlab-ci@localhost"
git config --global user.name "GitLab CI"
CI_REPOSITORY_URL="https://gitlab-ci:${CI_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"

API_BASE_URL="https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}"
MR_SOURCE_BRANCH=release

git checkout "${CI_COMMIT_BRANCH}"
npm install -g lerna
lerna version --no-push --yes

# If there is no new commit (number of commits between the current HEAD and the origin CI_COMMIT_BRANCH is 0)
#if [[ $(git rev-list --count HEAD..origin/"${CI_COMMIT_BRANCH}") -eq 0 ]]; then
#  echo "No new version, exiting"
#  exit 0
#fi

git checkout -B $MR_SOURCE_BRANCH
git push "${CI_REPOSITORY_URL}" --force

# Require a list of all the merge request and take a look if there is already one with the same source branch
MR_LIST=$(curl --silent "${API_BASE_URL}/merge_requests?state=opened&source_branch=${MR_SOURCE_BRANCH}" --header "Authorization: Bearer ${CI_ACCESS_TOKEN}")
# Parse iid from MR_LIST
MR_IID=$(echo "${MR_LIST}" | grep iid | sed -e 's/.*"iid":\([0-9]*\),.*/\1/')

# Get the title and description of the last commit
MR_TITLE=$(git log -1 --pretty=%s)
MR_DESCRIPTION=$(git log -1 --pretty=%b)

# If there is already a merge request (MR_IID is not empty)
if [[ -n $MR_IID ]]; then
  # Create a JSON body for the API call
  BODY=$(node -p "JSON.stringify({title: process.argv[1], description: process.argv[2]})" "${MR_TITLE}" "${MR_DESCRIPTION}")
  echo "BODY: ${BODY}"

  # Update the merge request with the new title and description
  curl --silent --show-error --fail \
    -X PUT "${API_BASE_URL}/merge_requests/${MR_IID}" \
    --header "Authorization: Bearer ${CI_ACCESS_TOKEN}" \
    --header "Content-Type: application/json" \
    --data "${BODY}"

  echo "Merge request $MR_IID updated with new title and description"

else
  # Create a JSON body for the API call
  BODY=$(node -p "JSON.stringify({source_branch: process.argv[1], target_branch: process.argv[2], title: process.argv[3], description: process.argv[4], remove_source_branch: true, squash_on_merge: false})" "${MR_SOURCE_BRANCH}" "${CI_DEFAULT_BRANCH}" "${MR_TITLE}" "${MR_DESCRIPTION}")
  echo "BODY: ${BODY}"

  # No MR found, let's create a new one
  curl --silent --show-error --fail \
    -X POST "${API_BASE_URL}/merge_requests" \
    --header "Authorization: Bearer ${CI_ACCESS_TOKEN}" \
    --header "Content-Type: application/json" \
    --data "${BODY}"

  echo "New merge request has been created"

fi
