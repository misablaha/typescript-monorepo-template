# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.8](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@app/api@1.0.7...@app/api@1.0.8) (2023-07-28)

**Note:** Version bump only for package @app/api





## [1.0.7](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@app/api@1.0.6...@app/api@1.0.7) (2023-07-27)

**Note:** Version bump only for package @app/api





## [1.0.6](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@app/api@1.0.5...@app/api@1.0.6) (2023-07-27)

**Note:** Version bump only for package @app/api





## [1.0.5](https://gitlab.com/misablaha/typescript-monorepo-template/compare/@app/api@1.0.4...@app/api@1.0.5) (2023-07-27)

**Note:** Version bump only for package @app/api





## 1.0.4 (2023-07-24)

**Note:** Version bump only for package @app/api

## [1.0.3](https://gitlab.com/misablaha/typescript-monorepo-template/compare/api@1.0.2...api@1.0.3) (2023-07-16)

**Note:** Version bump only for package api

## [1.0.2](https://gitlab.com/misablaha/typescript-monorepo-template/compare/api@1.0.1...api@1.0.2) (2023-07-16)

**Note:** Version bump only for package api

## [1.0.1](https://gitlab.com/misablaha/typescript-monorepo-template/compare/api@1.0.0...api@1.0.1) (2023-07-16)

**Note:** Version bump only for package api

# 1.0.0 (2023-07-16)

**Note:** Version bump only for package api
