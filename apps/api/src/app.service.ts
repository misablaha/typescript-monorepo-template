import { IUtilsService } from '@monorepo/ip-module'
import { Injectable } from '@nestjs/common'

@Injectable()
export class AppService {
  constructor(private utilsService: IUtilsService) {}

  async getHello(): Promise<string> {
    // return 'Hello from AppService'
    return `${this.utilsService.sayHello()}\nYour IP is: ${await this.utilsService.getIP()}`
  }
}
