import { IUtilsService } from '@monorepo/ip-module'
import { Controller, Get } from '@nestjs/common'

@Controller('me')
export class MeController {
  constructor(private utilsService: IUtilsService) {}

  @Get()
  async getHello(): Promise<string> {
    return `${this.utilsService.sayHello()}\nYour IP is: ${await this.utilsService.getIP()}`
  }
}
