import { UtilsModule } from '@monorepo/ip-module'
import { Module } from '@nestjs/common'
import { MeController } from './me.controller'

@Module({
  imports: [UtilsModule],
  controllers: [MeController],
})
export class MeModule {}
