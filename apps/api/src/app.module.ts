import { UtilsMemoizedService, UtilsModule } from '@monorepo/ip-module'
import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { MeModule } from './me/me.module'

@Module({
  imports: [UtilsModule.forRoot(UtilsMemoizedService), MeModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
