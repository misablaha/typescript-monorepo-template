## Description

Backend mono-repo using NestJS for APIs and packages for shared code.

## Code structure

- All dev dependencies are in root directory.
- All shared code is in `packages` directory.
- All APIs are in `apis` directory.
- Core TypeScript configuration is in root directory.
- The ESLint configuration is in root directory ONLY!
- ... as well as the Prettier configuration, gitignore, etc.

## Installation

```bash
$ yarn
```

## Development

NestJS APIs require packages to be built before running the server.  
To watch for changes in packages and build them automatically, run in root directory in separate terminal:
```bash
$ yarn build:watch
```

To run the server in development mode, run in specific API directory:
```bash
$ yarn start:dev
```

## Linting

To lint all packages is not needed to build them first.
Just run in root directory:
```bash
$ yarn lint
```

## Build

Repository is using **Turbo** together with **Yarn Workspaces**.  
This turbo pipeline:
```
  "build": {
    "dependsOn": ["^build"],
  }
```
says:
> When you call `turbo build` in root directory, call `build` in all workspaces
> and if there is local dependency on other workspace,
> call `build` for that workspace first.

## Release version

To release new version of package
- be sure all changes are committed (use [conventional commits](https://www.conventionalcommits.org/) notation)
- run in root directory:
```bash
$ yarn release
```
- if everything is OK, push changes to remote repository
```bash
$ git push --follow-tags origin master
```
We are using lerna to resolve dependencies between packages and bump versions.
- Lerna automatically resolves commit messages and bump versions accordingly.
- If there are dependencies between packages, lerna will automatically bump versions of dependent packages.

... test CI/CD pipeline 2
