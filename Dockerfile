############
## base image
############
FROM node:18.12.1-alpine AS base
WORKDIR /app

############
## build the app
###########
FROM base AS build

# add all package.json and yarn.lock files
# due to it is not possible to copy nested files from parent directory
# first tar them and then untar in the build stage
# tar --exclude node_modules -cf package.json.tar **/package.json **/yarn.lock
ADD ./package.json.tar .
# install dependencies
RUN yarn install
# copy the rest of the apps
COPY ./turbo.json ./tsconfig.json ./
COPY ./apps apps
COPY ./packages packages
# build apps
RUN yarn build
# drops dev dependencies from node_modules
RUN yarn install --production

FROM base AS release
ENV NODE_ENV=production
# copy only the built files
COPY --from=build /app .
